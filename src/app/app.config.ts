import {
    ApplicationConfig,
    importProvidersFrom,
    InjectionToken,
} from '@angular/core'
import { provideRouter } from '@angular/router'

import { routes } from './app.routes'
import { WeatherProvider } from './core/weather-provider.interface'
import { HttpClientModule } from '@angular/common/http'
import { OpenmeteoClientService } from './services/openmeteo-client.service'

export const WEATHER_PROVIDER: InjectionToken<WeatherProvider> =
    new InjectionToken('WeatherProvider')

export const appConfig: ApplicationConfig = {
    providers: [
        provideRouter(routes),
        importProvidersFrom(HttpClientModule),
        { provide: WEATHER_PROVIDER, useClass: OpenmeteoClientService },
    ],
}
