import { Component } from '@angular/core'

@Component({
    selector: 'app-landing-message',
    standalone: true,
    imports: [],
    templateUrl: './landing-message.component.html',
    styles: ``,
})
export class LandingMessageComponent {}
