import { Component, computed, inject } from '@angular/core'
import { WEATHER_PROVIDER } from '../../app.config'

@Component({
    selector: 'app-notfound-message',
    standalone: true,
    imports: [],
    templateUrl: './not-found-message.component.html',
})
export class NotFoundMessageComponent {
    // Data sources
    private weatherProvider = inject(WEATHER_PROVIDER)

    // Computed properties
    readonly location = computed((): string => {
        const state = this.weatherProvider.state()
        if (state.status === 'NOT_FOUND') return state.location
        return 'unknown location'
    })
}
