import { Component, inject } from '@angular/core'
import { WeatherProvider } from '../../core/weather-provider.interface'
import { WEATHER_PROVIDER } from '../../app.config'

@Component({
    selector: 'app-searchbox',
    standalone: true,
    imports: [],
    templateUrl: './searchbox.component.html',
})
export class SearchboxComponent {
    private weatherProvider: WeatherProvider = inject(WEATHER_PROVIDER)

    searchWeatherForLocation(location: string) {
        this.weatherProvider.retrieveWeatherForLocation(location).finally()
        console.log(`Launched search for weather for location: ${location}`)
    }
}
