import { Component } from '@angular/core'

@Component({
    selector: 'app-searching-message',
    standalone: true,
    imports: [],
    templateUrl: './searching-message.component.html',
})
export class SearchingMessageComponent {}
