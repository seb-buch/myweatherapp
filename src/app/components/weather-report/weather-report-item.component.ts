import { Component, Input } from '@angular/core'

@Component({
    selector: 'app-weather-report-item',
    standalone: true,
    imports: [],
    templateUrl: './weather-report-item.component.html',
    styleUrl: './weather-report-item.component.css',
})
export class WeatherReportItemComponent {
    @Input() itemValue: string | undefined = undefined
    @Input() iconClass = 'fa-solid fa-question'
    @Input() iconLabel = 'Unknown'
    @Input() tooltip = 'No information available'
}
