import { Component, computed, inject } from '@angular/core'
import { WeatherReportItemComponent } from './weather-report-item.component'
import { WeatherProvider } from '../../core/weather-provider.interface'
import { WEATHER_PROVIDER } from '../../app.config'
import { Weather } from '../../core/weather.model'
import {
    WeatherCategory,
    WeatherConditions,
} from '../../core/weather-conditions.model'
import { formatNumber } from '@angular/common'

type ReportedConditions = {
    description: string
    icon: string
}

type ReportedWeather = {
    location: string
    condition: ReportedConditions
    temperature: string
    humidity: string
    wind_speed: string
    cloud_coverage: string
    sunrise: string
    sunset: string
}

@Component({
    selector: 'app-weather-report',
    standalone: true,
    imports: [WeatherReportItemComponent],
    templateUrl: './weather-report.component.html',
})
export class WeatherReportComponent {
    private weatherProvider: WeatherProvider = inject(WEATHER_PROVIDER)
    weather = computed((): ReportedWeather | undefined => {
        const state = this.weatherProvider.state()
        if (state.status === 'OK') {
            return this.weatherToReportedWeather(state.weather)
        }
        return undefined
    })

    private formatValue(
        value: string | { value: number; unit: string } | Date
    ): string {
        if (typeof value === 'string') return value
        if (value instanceof Date)
            return `${value.getHours()}:${formatNumber(value.getMinutes(), 'en', '2.0')}`
        return `${value.value}\xa0${value.unit}`
    }

    private formatConditions(
        conditions: WeatherConditions
    ): ReportedConditions {
        return {
            description: conditions.description,
            icon: this.weatherCategoryToIcon(conditions.category),
        }
    }

    private weatherCategoryToIcon(category: WeatherCategory): string {
        const iconPrefix = 'wi wi-'

        switch (category) {
            case WeatherCategory.CLEAR:
                return `${iconPrefix}day-sunny`
            case WeatherCategory.CLOUDY:
                return `${iconPrefix}cloudy`
            case WeatherCategory.RAIN:
                return `${iconPrefix}rain`
            case WeatherCategory.DRIZZLE:
                return `${iconPrefix}sprinkle`
            case WeatherCategory.FOG:
                return `${iconPrefix}fog`
            case WeatherCategory.FREEZING_DRIZZLE:
                return `${iconPrefix}rain-mix`
            case WeatherCategory.FREEZING_RAIN:
                return `${iconPrefix}rain-mix`
            case WeatherCategory.RAIN_SHOWERS:
                return `${iconPrefix}showers`
            case WeatherCategory.SNOW:
                return `${iconPrefix}snow`
            case WeatherCategory.SNOW_GRAINS:
                return `${iconPrefix}snowflake-cold`
            case WeatherCategory.SNOW_SHOWERS:
                return `${iconPrefix}snow-wind`
            case WeatherCategory.THUNDERSTORM:
                return `${iconPrefix}thunderstorm`
            case WeatherCategory.THUNDERSTORM_WITH_HAIL:
                return `${iconPrefix}storm-showers`
            case WeatherCategory.UNKNOWN:
                return `${iconPrefix}na`
        }
    }

    private weatherToReportedWeather(weather: Weather): ReportedWeather {
        return {
            condition: this.formatConditions(weather.currentConditions),
            location: weather.location,
            temperature: this.formatValue(weather.temperature),
            humidity: this.formatValue(weather.relativeHumidity),
            wind_speed: this.formatValue(weather.windSpeed),
            cloud_coverage: this.formatValue(weather.cloudCoverage),
            sunrise: this.formatValue(weather.sunrise),
            sunset: this.formatValue(weather.sunset),
        }
    }
}
