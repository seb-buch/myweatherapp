export enum WeatherCategory {
    CLEAR = 'CLEAR',
    CLOUDY = 'CLOUDY',
    FOG = 'FOG',
    DRIZZLE = 'DRIZZLE',
    FREEZING_DRIZZLE = 'FREEZING_DRIZZLE',
    RAIN = 'RAIN',
    FREEZING_RAIN = 'FREEZING_RAIN',
    SNOW = 'SNOW',
    SNOW_GRAINS = 'SNOW_GRAINS',
    RAIN_SHOWERS = 'RAIN_SHOWERS',
    SNOW_SHOWERS = 'SNOW_SHOWERS',
    THUNDERSTORM = 'THUNDERSTORM',
    THUNDERSTORM_WITH_HAIL = 'THUNDERSTORM_WITH_HAIL',
    UNKNOWN = 'UNKNOWN',
}

// see https://open-meteo.com/en/docs/ for the WMO weather codes and descriptions
export class WeatherConditions {
    static readonly CLEAR = new WeatherConditions(
        WeatherCategory.CLEAR,
        'Clear sky'
    )
    static readonly MAINLY_CLEAR = new WeatherConditions(
        WeatherCategory.CLOUDY,
        'Mainly clear'
    )
    static readonly PARTLY_CLOUDY = new WeatherConditions(
        WeatherCategory.CLOUDY,
        'Partly cloudy'
    )
    static readonly OVERCAST = new WeatherConditions(
        WeatherCategory.CLOUDY,
        'Overcast'
    )
    static readonly FOG = new WeatherConditions(WeatherCategory.FOG, 'Fog')
    static readonly RIME_FOG = new WeatherConditions(
        WeatherCategory.FOG,
        'Depositing rime fog'
    )
    static readonly LIGHT_DRIZZLE = new WeatherConditions(
        WeatherCategory.DRIZZLE,
        'Light drizzle'
    )
    static readonly MODERATE_DRIZZLE = new WeatherConditions(
        WeatherCategory.DRIZZLE,
        'Moderate drizzle'
    )
    static readonly DENSE_DRIZZLE = new WeatherConditions(
        WeatherCategory.DRIZZLE,
        'Dense drizzle'
    )
    static readonly LIGHT_FREEZING_DRIZZLE = new WeatherConditions(
        WeatherCategory.FREEZING_DRIZZLE,
        'Light freezing drizzle'
    )
    static readonly DENSE_FREEZING_DRIZZLE = new WeatherConditions(
        WeatherCategory.FREEZING_DRIZZLE,
        'Dense freezing drizzle'
    )
    static readonly SLIGHT_RAIN = new WeatherConditions(
        WeatherCategory.RAIN,
        'Slight rain'
    )
    static readonly MODERATE_RAIN = new WeatherConditions(
        WeatherCategory.RAIN,
        'Moderate rain'
    )
    static readonly HEAVY_RAIN = new WeatherConditions(
        WeatherCategory.RAIN,
        'Heavy rain'
    )
    static readonly LIGHT_FREEZING_RAIN = new WeatherConditions(
        WeatherCategory.FREEZING_RAIN,
        'Light freezing rain'
    )
    static readonly HEAVY_FREEZING_RAIN = new WeatherConditions(
        WeatherCategory.FREEZING_RAIN,
        'Heavy freezing rain'
    )
    static readonly SLIGHT_SNOW = new WeatherConditions(
        WeatherCategory.SNOW,
        'Slight snow'
    )
    static readonly MODERATE_SNOW = new WeatherConditions(
        WeatherCategory.SNOW,
        'Moderate snow'
    )
    static readonly HEAVY_SNOW = new WeatherConditions(
        WeatherCategory.SNOW,
        'Heavy snow'
    )
    static readonly SNOW_GRAINS = new WeatherConditions(
        WeatherCategory.SNOW_GRAINS,
        'Snow grains'
    )
    static readonly SLIGHT_RAIN_SHOWERS = new WeatherConditions(
        WeatherCategory.RAIN_SHOWERS,
        'Slight rain showers'
    )
    static readonly MODERATE_RAIN_SHOWERS = new WeatherConditions(
        WeatherCategory.RAIN_SHOWERS,
        'Moderate rain showers'
    )
    static readonly VIOLENT_RAIN_SHOWERS = new WeatherConditions(
        WeatherCategory.RAIN_SHOWERS,
        'Violent rain showers'
    )
    static readonly SLIGHT_SNOW_SHOWERS = new WeatherConditions(
        WeatherCategory.SNOW_SHOWERS,
        'Slight snow showers'
    )
    static readonly HEAVY_SNOW_SHOWERS = new WeatherConditions(
        WeatherCategory.SNOW_SHOWERS,
        'Heavy snow showers'
    )
    static readonly SLIGHT_THUNDERSTORM = new WeatherConditions(
        WeatherCategory.THUNDERSTORM,
        'Slight thunderstorm'
    )
    static readonly SLIGHT_THUNDERSTORM_WITH_HAIL = new WeatherConditions(
        WeatherCategory.THUNDERSTORM_WITH_HAIL,
        'Slight thunderstorm with hail'
    )
    static readonly HEAVY_THUNDERSTORM_WITH_HAIL = new WeatherConditions(
        WeatherCategory.THUNDERSTORM_WITH_HAIL,
        'Heavy thunderstorm with hail'
    )
    static readonly UNKNOWN = new WeatherConditions(
        WeatherCategory.UNKNOWN,
        'Unknown weather condition'
    )

    private constructor(
        readonly category: WeatherCategory,
        readonly description: string
    ) {}

    static fromWMOCode(code: number): WeatherConditions {
        switch (code) {
            case 0:
                return WeatherConditions.CLEAR
            case 1:
                return WeatherConditions.MAINLY_CLEAR
            case 2:
                return WeatherConditions.PARTLY_CLOUDY
            case 3:
                return WeatherConditions.OVERCAST
            case 45:
                return WeatherConditions.FOG
            case 48:
                return WeatherConditions.RIME_FOG
            case 51:
                return WeatherConditions.LIGHT_DRIZZLE
            case 53:
                return WeatherConditions.MODERATE_DRIZZLE
            case 55:
                return WeatherConditions.DENSE_DRIZZLE
            case 56:
                return WeatherConditions.LIGHT_FREEZING_DRIZZLE
            case 57:
                return WeatherConditions.DENSE_FREEZING_DRIZZLE
            case 61:
                return WeatherConditions.SLIGHT_RAIN
            case 63:
                return WeatherConditions.MODERATE_RAIN
            case 65:
                return WeatherConditions.HEAVY_RAIN
            case 66:
                return WeatherConditions.LIGHT_FREEZING_RAIN
            case 67:
                return WeatherConditions.HEAVY_FREEZING_RAIN
            case 71:
                return WeatherConditions.SLIGHT_SNOW
            case 73:
                return WeatherConditions.MODERATE_SNOW
            case 75:
                return WeatherConditions.HEAVY_SNOW
            case 77:
                return WeatherConditions.SNOW_GRAINS
            case 80:
                return WeatherConditions.SLIGHT_RAIN_SHOWERS
            case 81:
                return WeatherConditions.MODERATE_RAIN_SHOWERS
            case 82:
                return WeatherConditions.VIOLENT_RAIN_SHOWERS
            case 85:
                return WeatherConditions.SLIGHT_SNOW_SHOWERS
            case 86:
                return WeatherConditions.HEAVY_SNOW_SHOWERS
            case 95:
                return WeatherConditions.SLIGHT_THUNDERSTORM
            case 96:
                return WeatherConditions.SLIGHT_THUNDERSTORM_WITH_HAIL
            case 99:
                return WeatherConditions.HEAVY_THUNDERSTORM_WITH_HAIL
            default:
                console.warn(`Unknown WMO weather code: ${code}`)
                return WeatherConditions.UNKNOWN
        }
    }
}
