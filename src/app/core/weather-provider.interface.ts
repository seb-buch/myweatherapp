import { Weather } from './weather.model'
import { Signal } from '@angular/core'

export enum WeatherProviderStatus {
    INITIALIZED = 'INITIALIZED',
    SEARCHING = 'SEARCHING',
    OK = 'OK',
    NOT_FOUND = 'NOT_FOUND',
}

export type WeatherProviderState =
    | {
          status: WeatherProviderStatus.INITIALIZED
      }
    | {
          status: WeatherProviderStatus.OK
          weather: Weather
      }
    | {
          status: WeatherProviderStatus.SEARCHING
          location: string
      }
    | {
          status: WeatherProviderStatus.NOT_FOUND
          location: string
      }

export interface WeatherProvider {
    state: Signal<WeatherProviderState>

    retrieveWeatherForLocation(location: string): Promise<Weather | undefined>
}
