import { WeatherConditions } from './weather-conditions.model'

export type ValueWithUnits = {
    value: number
    unit: string
}

export type Weather = {
    location: string
    currentConditions: WeatherConditions
    temperature: ValueWithUnits
    relativeHumidity: ValueWithUnits
    cloudCoverage: ValueWithUnits
    windSpeed: ValueWithUnits
    sunrise: Date
    sunset: Date
    isDay: boolean
}
