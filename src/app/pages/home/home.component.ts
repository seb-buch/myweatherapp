import { Component, computed, inject } from '@angular/core'
import { SearchboxComponent } from '../../components/searchbox/searchbox.component'
import { WeatherReportComponent } from '../../components/weather-report/weather-report.component'
import { LandingMessageComponent } from '../../components/landing-message/landing-message.component'
import { WEATHER_PROVIDER } from '../../app.config'
import {
    WeatherProvider,
    WeatherProviderStatus,
} from '../../core/weather-provider.interface'
import { NgIf } from '@angular/common'
import { NotFoundMessageComponent } from '../../components/notfound-message/not-found-message.component'
import { SearchingMessageComponent } from '../../components/searching-message/searching-message.component'

@Component({
    selector: 'app-home',
    standalone: true,
    imports: [
        SearchboxComponent,
        WeatherReportComponent,
        LandingMessageComponent,
        NotFoundMessageComponent,
        SearchingMessageComponent,
        NgIf,
    ],
    templateUrl: './home.component.html',
})
export class HomeComponent {
    // Data sources
    private weatherProvider: WeatherProvider = inject(WEATHER_PROVIDER)

    // Computed properties
    private status = computed(
        (): WeatherProviderStatus => this.weatherProvider.state().status
    )
    show_report = computed(
        (): boolean => this.status() === WeatherProviderStatus.OK
    )
    show_welcome = computed(
        (): boolean => this.status() === WeatherProviderStatus.INITIALIZED
    )
    show_notfound = computed(
        (): boolean => this.status() === WeatherProviderStatus.NOT_FOUND
    )
    show_searching = computed(
        (): boolean => this.status() === WeatherProviderStatus.SEARCHING
    )
}
