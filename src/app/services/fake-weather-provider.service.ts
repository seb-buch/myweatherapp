import {
    WeatherProvider,
    WeatherProviderState,
    WeatherProviderStatus,
} from '../core/weather-provider.interface'
import { computed, Signal, signal, WritableSignal } from '@angular/core'
import { Weather } from '../core/weather.model'
import { WeatherConditions } from '../core/weather-conditions.model'

const FAKE_WEATHER: Weather = {
    currentConditions: WeatherConditions.CLEAR,
    location: 'unknown',
    relativeHumidity: { value: 50, unit: '%' },
    sunrise: new Date('2024-03-01T07:29'),
    sunset: new Date('2024-03-01T18:43'),
    temperature: { value: 14, unit: '°C' },
    windSpeed: { value: 10, unit: 'km/h' },
    cloudCoverage: { value: 2, unit: '%' },
    isDay: true,
}

export class FakeWeatherProviderService implements WeatherProvider {
    private location: WritableSignal<string | undefined> = signal(undefined)

    state: Signal<WeatherProviderState> = computed((): WeatherProviderState => {
        const location = this.location()

        if (location === undefined || location === '') {
            return {
                status: WeatherProviderStatus.INITIALIZED,
            }
        }

        if (location === 'nonexistent') {
            return {
                status: WeatherProviderStatus.NOT_FOUND,
                location,
            }
        }

        return {
            status: WeatherProviderStatus.OK,
            weather: {
                ...FAKE_WEATHER,
                location,
            },
        }
    })

    retrieveWeatherForLocation(location: string): Promise<Weather | undefined> {
        this.location.set(location)

        const state = this.state()

        if (state.status === WeatherProviderStatus.OK)
            return Promise.resolve(state.weather)
        return Promise.resolve(undefined)
    }
}
