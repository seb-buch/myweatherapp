import { TestBed } from '@angular/core/testing'
import { OpenmeteoClientService } from './openmeteo-client.service'
import {
    HttpClientTestingModule,
    HttpTestingController,
} from '@angular/common/http/testing'
import { WeatherConditions } from '../core/weather-conditions.model'
import { Weather } from '../core/weather.model'
import { WeatherProviderStatus } from '../core/weather-provider.interface'

function flushDefaultRequests(httpTestingController: HttpTestingController) {
    const geodingReq = httpTestingController.expectOne(
        TEST_DATA.geocoding_api_call
    )
    geodingReq.flush(TEST_DATA.gecoding_api_response)
    const weatherReq = httpTestingController.expectOne(
        TEST_DATA.weather_api_call
    )
    weatherReq.flush(TEST_DATA.weather_api_response)
}

function flushNonexistentLocationRequest(
    httpTestingController: HttpTestingController,
    location: string
) {
    const geodingReq = httpTestingController.expectOne(
        `https://geocoding-api.open-meteo.com/v1/search?name=${location}&count=1&language=en&format=json`
    )
    // Excerpt from https://geocoding-api.open-meteo.com/v1/search?name=nonexistent&count=1&language=en&format=json
    geodingReq.flush({})
    // No weather request!
}

describe('OpenmeteoClientService', () => {
    let httpTestingController: HttpTestingController
    let service: OpenmeteoClientService

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: OpenmeteoClientService,
                    useClass: OpenmeteoClientService,
                },
            ],
            imports: [HttpClientTestingModule],
        })
        service = TestBed.inject(OpenmeteoClientService)
        // Inject the test controller for each test
        httpTestingController = TestBed.inject(HttpTestingController)
    })

    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify()
    })

    it('should have status=INITIALIZED on init', async () => {
        // Given an OpenMeteoClientService
        // Then the status should be INITIALIZED
        expect(service.state().status).toEqual(
            WeatherProviderStatus.INITIALIZED
        )
    })

    it('should retrieve weather for a location', async () => {
        // Given an OpenMeteoClientService
        // When we retrieve the weather for a location
        const weatherPromise = service.retrieveWeatherForLocation(
            TEST_DATA.location
        )
        flushDefaultRequests(httpTestingController)
        const weather = await weatherPromise

        // Then the weather should be as expected
        expect(weather).toEqual(TEST_DATA.weather)
        // And the status should be OK
        const state = service.state()
        expect(state.status).toEqual(WeatherProviderStatus.OK)
        // And the weather stored in state should be the same as the one returned
        expect(
            state.status === WeatherProviderStatus.OK
                ? state.weather
                : undefined
        ).toEqual(TEST_DATA.weather)
    })

    it('should behave when location is not found', async () => {
        const LOCATION = 'nonexistent'
        // Given an OpenMeteoClientService
        // When we retrieve the weather for a location that does not exist
        const weatherPromise = service.retrieveWeatherForLocation(LOCATION)
        flushNonexistentLocationRequest(httpTestingController, LOCATION)
        const weather = await weatherPromise

        // Then the weather should be undefined
        expect(weather).toBeUndefined()
        // And the status should be NOT_FOUND
        const state = service.state()
        expect(state.status).toEqual(WeatherProviderStatus.NOT_FOUND)
        // And the location stored in state should be the same as the one requested
        expect(
            state.status === WeatherProviderStatus.NOT_FOUND
                ? state.location
                : undefined
        ).toEqual(LOCATION)
    })

    it('should default to initiliazed state if location is empty', async () => {
        // Given an OpenMeteoClientService
        // When we retrieve the weather for an empty location
        const weatherPromise = service.retrieveWeatherForLocation('')
        // Then the weather should be undefined
        const weather = await weatherPromise
        expect(weather).toBeUndefined()
        // And the status should be INITIALIZED
        const state = service.state()
        expect(state.status).toEqual(WeatherProviderStatus.INITIALIZED)
    })

    it('should have status=SEARCHING while retrieving weather', async () => {
        // Given an OpenMeteoClientService
        // When we retrieve the weather for a location
        const weatherPromise = service.retrieveWeatherForLocation(
            TEST_DATA.location
        )
        // Then the status should be SEARCHING
        const state = service.state()
        expect(state.status).toEqual(WeatherProviderStatus.SEARCHING)
        // And the location stored in state should be the same as the one requested
        expect(
            state.status === WeatherProviderStatus.SEARCHING
                ? state.location
                : undefined
        ).toBe(TEST_DATA.location)
        flushDefaultRequests(httpTestingController)
        await weatherPromise
    })
})

const TEST_DATA = {
    location: 'Toulouse',
    geocoding_api_call:
        'https://geocoding-api.open-meteo.com/v1/search?name=Toulouse&count=1&language=en&format=json',
    // Excerpt from https://geocoding-api.open-meteo.com/v1/search?name=Toulouse&count=1&language=en&format=json
    gecoding_api_response: {
        results: [
            {
                name: 'Toulouse',
                latitude: 43.60426,
                longitude: 1.44367,
                country_code: 'FR',
            },
        ],
    },
    weather_api_call:
        'https://api.open-meteo.com/v1/forecast?latitude=43.6043&longitude=1.4437&current=temperature_2m,relative_humidity_2m,apparent_temperature,is_day,weather_code,cloud_cover,wind_speed_10m,wind_direction_10m&daily=sunrise,sunset&timezone=auto&forecast_days=1',
    // Excerpt from https://api.open-meteo.com/v1/forecast?latitude=43.6043&longitude=1.4437&current=temperature_2m,relative_humidity_2m,apparent_temperature,is_day,weather_code,cloud_cover,wind_speed_10m,wind_direction_10m&daily=sunrise,sunset&timezone=auto&forecast_days=1
    weather_api_response: {
        latitude: 43.6,
        longitude: 1.44,
        utc_offset_seconds: 3600,
        timezone: 'Europe/Paris',
        timezone_abbreviation: 'CET',
        current_units: {
            temperature_2m: '°C',
            relative_humidity_2m: '%',
            apparent_temperature: '°C',
            is_day: '',
            weather_code: 'wmo code',
            cloud_cover: '%',
            wind_speed_10m: 'km/h',
            wind_direction_10m: '°',
        },
        current: {
            temperature_2m: 16.0,
            relative_humidity_2m: 57,
            apparent_temperature: 14.1,
            is_day: 1,
            weather_code: 3,
            cloud_cover: 42,
            wind_speed_10m: 9.3,
            wind_direction_10m: 126,
        },
        daily_units: { time: 'iso8601', sunrise: 'iso8601', sunset: 'iso8601' },
        daily: {
            sunrise: ['2024-03-01T07:28'],
            sunset: ['2024-03-01T18:44'],
        },
    },
    weather: {
        isDay: true,
        location: 'Toulouse, FR',
        temperature: { value: 16.0, unit: '°C' },
        relativeHumidity: { value: 57, unit: '%' },
        windSpeed: { value: 9.3, unit: 'km/h' },
        currentConditions: WeatherConditions.fromWMOCode(3),
        sunrise: new Date('2024-03-01T07:28'),
        sunset: new Date('2024-03-01T18:44'),
        cloudCoverage: { value: 42, unit: '%' },
    } as Weather,
}
