import { computed, inject, signal, Signal, WritableSignal } from '@angular/core'
import {
    WeatherProvider,
    WeatherProviderState,
    WeatherProviderStatus,
} from '../core/weather-provider.interface'
import { Weather } from '../core/weather.model'
import { HttpClient } from '@angular/common/http'
import { WeatherConditions } from '../core/weather-conditions.model'
import { catchError, map, Observable, of, switchMap, zip } from 'rxjs'

class NotFoundError extends Error {}

type OpenMeteoGeocodingData = {
    results: {
        name: string
        latitude: number
        longitude: number
        country_code: string
    }[]
}

type GeocodingData = {
    name: string
    latitude: number
    longitude: number
}

type OpenMeteoWeatherData = {
    current: {
        temperature_2m: number
        relative_humidity_2m: number
        is_day: number
        weather_code: number
        cloud_cover: number
        wind_speed_10m: number
        wind_direction_10m: number
    }
    current_units: {
        temperature_2m: string
        relative_humidity_2m: string
        apparent_temperature: string
        wind_speed_10m: string
        cloud_cover: string
    }
    daily: {
        sunrise: string[]
        sunset: string[]
    }
}

type APIResponse<T> = T

function roundTo4Decimals(value: number): number {
    return Math.round((value + Number.EPSILON) * 10000) / 10000
}

function openMeteoGeocodingToInternalGeocoding(
    rawData: OpenMeteoGeocodingData
) {
    if (!('results' in rawData) || rawData.results.length === 0) {
        throw new NotFoundError()
    }
    const data = rawData.results[0]
    return {
        name: `${data.name}, ${data.country_code}`,
        latitude: roundTo4Decimals(data.latitude),
        longitude: roundTo4Decimals(data.longitude),
    }
}

function openMeteoWeatherToWeather(
    rawData: OpenMeteoWeatherData,
    geocodingData: GeocodingData
) {
    const currentData = rawData.current
    const currentUnits = rawData.current_units

    return {
        isDay: currentData.is_day === 1,
        location: geocodingData.name,
        temperature: {
            value: currentData.temperature_2m,
            unit: currentUnits.temperature_2m,
        },
        relativeHumidity: {
            value: currentData.relative_humidity_2m,
            unit: currentUnits.relative_humidity_2m,
        },
        windSpeed: {
            value: currentData.wind_speed_10m,
            unit: currentUnits.wind_speed_10m,
        },
        currentConditions: WeatherConditions.fromWMOCode(
            currentData.weather_code
        ),
        sunrise: new Date(rawData.daily.sunrise[0]),
        sunset: new Date(rawData.daily.sunset[0]),
        cloudCoverage: {
            value: currentData.cloud_cover,
            unit: currentUnits.cloud_cover,
        },
    }
}

const OPENMETEO_GEOCODING_API_URL =
    'https://geocoding-api.open-meteo.com/v1/search'
const OPENMETEO_WEATHER_API_URL = 'https://api.open-meteo.com/v1/forecast'

export class OpenmeteoClientService implements WeatherProvider {
    private weather: WritableSignal<Weather | undefined> = signal(undefined)
    private location: WritableSignal<string | undefined> = signal(undefined)
    private waitingForLocation: WritableSignal<boolean> = signal(false)
    state: Signal<WeatherProviderState> = computed((): WeatherProviderState => {
        const weather = this.weather()
        const location = this.location()
        const waitingForLocation = this.waitingForLocation()

        if (location === undefined) {
            return { status: WeatherProviderStatus.INITIALIZED }
        }

        if (waitingForLocation) {
            return {
                status: WeatherProviderStatus.SEARCHING,
                location: location,
            }
        }

        if (weather === undefined) {
            return {
                status: WeatherProviderStatus.NOT_FOUND,
                location: location,
            }
        }

        return {
            status: WeatherProviderStatus.OK,
            weather,
        }
    })
    private httpClient: HttpClient = inject(HttpClient)

    async retrieveWeatherForLocation(
        location: string
    ): Promise<Weather | undefined> {
        if (location === '') {
            this.location.set(undefined)
            return Promise.resolve(undefined)
        }

        this.location.set(location)
        return new Promise((resolve) => {
            this.getWeatherData(location).subscribe((response) => {
                this.waitingForLocation.set(false)
                resolve(response)
            })
        })
    }

    private getDataFromOpenMeteo<T>(
        url: string,
        params: string
    ): Observable<APIResponse<T>> {
        return this.httpClient.get<APIResponse<T>>(`${url}?${params}`)
    }

    private getGeocodingDataFromOpenMeteo(location: string) {
        const params = `name=${location}&count=1&language=en&format=json`
        return this.getDataFromOpenMeteo<OpenMeteoGeocodingData>(
            OPENMETEO_GEOCODING_API_URL,
            params
        )
    }

    private getWeatherDataFromOpenMeteo(geoCoordinates: {
        latitude: number
        longitude: number
    }) {
        let params = `latitude=${geoCoordinates.latitude}&longitude=${geoCoordinates.longitude}&current=temperature_2m,`
        params +=
            'relative_humidity_2m,apparent_temperature,is_day,weather_code,cloud_cover,wind_speed_10m,wind_direction_10m' +
            '&daily=sunrise,sunset&timezone=auto&forecast_days=1'
        return this.getDataFromOpenMeteo<OpenMeteoWeatherData>(
            OPENMETEO_WEATHER_API_URL,
            params
        )
    }

    private getWeatherData(location: string) {
        console.log('Retrieving weather for location:', location)
        this.waitingForLocation.set(true)

        return this.getGeocodingDataFromOpenMeteo(location).pipe(
            map((response: APIResponse<OpenMeteoGeocodingData>) => {
                const data: GeocodingData =
                    openMeteoGeocodingToInternalGeocoding(response)
                console.log(
                    `Geographical coordinates for ${location}: latitude=${data.latitude}, longitude=${data.longitude}`
                )
                return data
            }),
            switchMap((geocodingData: GeocodingData) => {
                return zip(
                    of(geocodingData),
                    this.getWeatherDataFromOpenMeteo(geocodingData)
                )
            }),
            map(
                ([geocodingData, response]: [
                    GeocodingData,
                    APIResponse<OpenMeteoWeatherData>,
                ]) => {
                    const data = openMeteoWeatherToWeather(
                        response,
                        geocodingData
                    )
                    this.weather.set(data)
                    console.log('Weather:', JSON.stringify(data))
                    return data
                }
            ),
            catchError((error) => {
                if (error instanceof NotFoundError) {
                    this.weather.set(undefined)
                    return of(undefined)
                }

                console.error('Error while retrieving weather:', error)
                return of(undefined)
            })
        )
    }
}
