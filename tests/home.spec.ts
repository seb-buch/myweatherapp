import { expect, test } from '@playwright/test'

test.describe('Homepage', () => {
    test('should have a title', async ({ page }) => {
        await page.goto('/')
        await expect(page.getByRole('heading')).toHaveText('MyWeatherApp')
    })

    test('should have a search input', async ({ page }) => {
        await page.goto('/')
        await expect(page.getByRole('search')).toBeVisible()
    })

    test('should have a search button', async ({ page }) => {
        await page.goto('/')
        await expect(
            page.getByRole('button', { name: 'Get Weather' })
        ).toBeVisible()
    })

    test('should display the welcome message by default', async ({ page }) => {
        await page.goto('/')
        await expect(page.getByText('Welcome')).toBeVisible()
    })
})
