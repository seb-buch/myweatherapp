import { expect, test } from '@playwright/test'

test.describe('Search feature', () => {
    test.beforeEach(async ({ page }) => {
        // Given the user is on the homepage
        await page.goto('/')
    })

    test('should allow to search for a weather in a location', async ({
        page,
    }) => {
        const TEST_LOCATION = 'Toulouse'

        // Given the user is on the homepage (done in beforeEach)
        // When the user types a location in the search input and clicks the 'get weather' button
        await page.getByPlaceholder('e.g. Toulouse').fill(TEST_LOCATION)
        await page.getByRole('button', { name: 'Get Weather' }).click()

        // Then the user should see the weather report for the location
        await expect(
            page.getByRole('heading', { name: 'Weather report' })
        ).toBeVisible()
        await expect(
            page.locator('section').filter({ hasText: 'The location is' })
        ).toContainText(TEST_LOCATION, { ignoreCase: true })
    })

    test('should display a not found message when the location is not found', async ({
        page,
    }) => {
        const TEST_LOCATION = 'nonexistent'

        // Given the user is on the homepage (done in beforeEach)
        // When the user types a nonexistent location in the search input and clicks the 'get weather' button
        await page.getByPlaceholder('e.g. Toulouse').fill(TEST_LOCATION)
        await page.getByRole('button', { name: 'Get Weather' }).click()

        // Then the user should see a not found message
        await expect(
            page.getByText(`${TEST_LOCATION} could not be found`)
        ).toBeVisible()
    })

    test('should display the welcome message when location is empty', async ({
        page,
    }) => {
        // Given the user is on the homepage (done in beforeEach)
        // When the user clicks the 'get weather' button without entering a location
        await page.getByRole('button', { name: 'Get Weather' }).click()

        // Then the user should see the welcome message
        await expect(page.getByText('Welcome')).toBeVisible()
    })
})
