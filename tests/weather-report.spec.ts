import { expect, test } from '@playwright/test'
import { WeatherConditions } from '../src/app/core/weather-conditions.model'

function getTestGeocodingResponse(location: string) {
    return {
        results: [
            {
                name: location,
                latitude: 43.60426,
                longitude: 1.44367,
                country_code: 'FR',
            },
        ],
    }
}

function getTestWeatherResponse(weatherCode?: number) {
    return {
        latitude: 43.6,
        longitude: 1.44,
        utc_offset_seconds: 3600,
        timezone: 'Europe/Paris',
        timezone_abbreviation: 'CET',
        current_units: {
            temperature_2m: '°C',
            relative_humidity_2m: '%',
            apparent_temperature: '°C',
            is_day: '',
            weather_code: 'wmo code',
            cloud_cover: '%',
            wind_speed_10m: 'km/h',
            wind_direction_10m: '°',
        },
        current: {
            temperature_2m: 16.0,
            relative_humidity_2m: 57,
            apparent_temperature: 14.1,
            is_day: 1,
            weather_code: weatherCode ?? 0,
            cloud_cover: 42,
            wind_speed_10m: 9.3,
            wind_direction_10m: 126,
        },
        daily_units: { time: 'iso8601', sunrise: 'iso8601', sunset: 'iso8601' },
        daily: {
            sunrise: ['2024-03-01T07:28'],
            sunset: ['2024-03-01T18:44'],
        },
    }
}

const KNOWN_WEATHER_CODES = [
    {
        code: 0,
        conditions: WeatherConditions.fromWMOCode(0),
        iconName: 'wi-day-sunny',
    },
    {
        code: 1,
        conditions: WeatherConditions.fromWMOCode(1),
        iconName: 'wi-cloudy',
    },
    {
        code: 2,
        conditions: WeatherConditions.fromWMOCode(2),
        iconName: 'wi-cloudy',
    },
    {
        code: 3,
        conditions: WeatherConditions.fromWMOCode(3),
        iconName: 'wi-cloudy',
    },
    {
        code: 45,
        conditions: WeatherConditions.fromWMOCode(45),
        iconName: 'wi-fog',
    },
    {
        code: 48,
        conditions: WeatherConditions.fromWMOCode(48),
        iconName: 'wi-fog',
    },
    {
        code: 51,
        conditions: WeatherConditions.fromWMOCode(51),
        iconName: 'wi-sprinkle',
    },
    {
        code: 53,
        conditions: WeatherConditions.fromWMOCode(53),
        iconName: 'wi-sprinkle',
    },
    {
        code: 55,
        conditions: WeatherConditions.fromWMOCode(55),
        iconName: 'wi-sprinkle',
    },
    {
        code: 56,
        conditions: WeatherConditions.fromWMOCode(56),
        iconName: 'wi-rain-mix',
    },
    {
        code: 57,
        conditions: WeatherConditions.fromWMOCode(57),
        iconName: 'wi-rain-mix',
    },
    {
        code: 61,
        conditions: WeatherConditions.fromWMOCode(61),
        iconName: 'wi-rain',
    },
    {
        code: 63,
        conditions: WeatherConditions.fromWMOCode(63),
        iconName: 'wi-rain',
    },
    {
        code: 65,
        conditions: WeatherConditions.fromWMOCode(65),
        iconName: 'wi-rain',
    },
    {
        code: 66,
        conditions: WeatherConditions.fromWMOCode(66),
        iconName: 'wi-rain-mix',
    },
    {
        code: 67,
        conditions: WeatherConditions.fromWMOCode(67),
        iconName: 'wi-rain-mix',
    },
    {
        code: 71,
        conditions: WeatherConditions.fromWMOCode(71),
        iconName: 'wi-snow',
    },
    {
        code: 73,
        conditions: WeatherConditions.fromWMOCode(73),
        iconName: 'wi-snow',
    },
    {
        code: 75,
        conditions: WeatherConditions.fromWMOCode(75),
        iconName: 'wi-snow',
    },
    {
        code: 77,
        conditions: WeatherConditions.fromWMOCode(77),
        iconName: 'wi-snowflake-cold',
    },
    {
        code: 80,
        conditions: WeatherConditions.fromWMOCode(80),
        iconName: 'wi-showers',
    },
    {
        code: 81,
        conditions: WeatherConditions.fromWMOCode(81),
        iconName: 'wi-showers',
    },
    {
        code: 82,
        conditions: WeatherConditions.fromWMOCode(82),
        iconName: 'wi-showers',
    },
    {
        code: 85,
        conditions: WeatherConditions.fromWMOCode(85),
        iconName: 'wi-snow-wind',
    },
    {
        code: 86,
        conditions: WeatherConditions.fromWMOCode(86),
        iconName: 'wi-snow-wind',
    },
    {
        code: 95,
        conditions: WeatherConditions.fromWMOCode(95),
        iconName: 'wi-thunderstorm',
    },
    {
        code: 96,
        conditions: WeatherConditions.fromWMOCode(96),
        iconName: 'wi-storm-showers',
    },
    {
        code: 99,
        conditions: WeatherConditions.fromWMOCode(99),
        iconName: 'wi-storm-showers',
    },
]
test.describe('Weather report', () => {
    test.beforeEach(async ({ page }) => {
        // Given the user is on the homepage
        await page.goto('/')
    })

    for (const {
        code: weatherCode,
        conditions,
        iconName,
    } of KNOWN_WEATHER_CODES) {
        test(`should display the proper icon for ${conditions.description}`, async ({
            page,
        }) => {
            // Test setup
            const location = 'Toulouse'
            await page.route('*/**/search?*', async (route) => {
                const json = getTestGeocodingResponse(location)
                await route.fulfill({ json })
            })
            const weatherResponse = getTestWeatherResponse(weatherCode)
            await page.route('*/**/forecast?*', async (route) => {
                const json = weatherResponse
                await route.fulfill({ json })
            })

            // Given the user is on the homepage (done in beforeEach)
            // When the user types a location in the search input and clicks the 'get weather' button
            await page.getByPlaceholder('e.g. Toulouse').fill('Toulouse')
            await page.getByRole('button', { name: 'Get Weather' }).click()

            // Then the user should see the weather report for the location
            await expect(
                page.getByRole('heading', { name: 'Weather report' })
            ).toBeVisible()

            // And the user should see the current weather conditions with the proper icon
            const currentWeatherConditions = page
                .locator('section')
                .filter({ hasText: 'Current weather conditions:' })
            const icon = currentWeatherConditions.locator('i').first()
            await expect(currentWeatherConditions).toBeVisible()
            await expect(icon).toHaveAttribute('title', conditions.description)
            await expect(icon).toHaveAttribute('class', `wi ${iconName}`)
        })
    }
})
